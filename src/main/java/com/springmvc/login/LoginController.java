package com.springmvc.login;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {

    @RequestMapping(value="/login", method= RequestMethod.GET)
    public String showLoginPage(@RequestParam(value="error", required = false) boolean error, @RequestParam(value="logout", required= false) boolean logout, ModelMap modelMap) {

        String errMsg = "";

        if (error) {
            errMsg = "Username or password is incorrect";
        }

        if (logout) {
            errMsg = "You have been sucessfully logget out";
        }
        modelMap.addAttribute("errMsg", errMsg);
        return "login";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout=true";
    }

}

package com.springmvc.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class TodoController {

    @Autowired
    TodoService service = TodoService.getInstance();

    public String getUserName() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            return ((UserDetails)principal).getUsername();
        } else {
            return principal.toString();
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }

    @RequestMapping(value="/todo", method= RequestMethod.GET)
    public String todoList(ModelMap modelMap) {
        modelMap.addAttribute("todoList", service.retrieveItems(getUserName()));
        return "todo";
    }

    @RequestMapping(value="/add-todo", method= RequestMethod.GET)
    public String showAddTodo(ModelMap modelMap) {
        modelMap.addAttribute("todo", new Todo(0,  "Default description", new Date(), false, getUserName()));
        modelMap.addAttribute("action", "todo");
        return "add-todo";
    }

    @RequestMapping(value="/add-todo", method= RequestMethod.POST)
    public String addTodo(@Valid Todo todo, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "add-todo";
        }
        service.addTodo(todo.getDescription(), new Date(), false, getUserName());
        return "redirect:todo";
    }

    @RequestMapping(value="/edit-todo", method= RequestMethod.GET)
    public String showEditTodo(@RequestParam int id, ModelMap modelMap) {
        Todo todo = service.retrieveItem(id);
        modelMap.addAttribute("todo", todo);
        return "add-todo";
    }

    @RequestMapping(value="/edit-todo", method= RequestMethod.POST)
    public String editTodo(@Valid Todo todo, BindingResult result) {
        if (result.hasErrors()) {
            return "add-todo";
        }

        todo.setUser(getUserName());
        service.editTodo(todo);

        return "redirect:todo";

    }

    @RequestMapping(value="/delete-todo", method= RequestMethod.GET)
    public String deleteTodo(@RequestParam int id) {

        service.deleteItemById(id);
        return "redirect:todo";
    }




}

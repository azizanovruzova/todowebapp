package com.springmvc.todo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Service
public class TodoService {

    private static List<Todo> todoList = new ArrayList<>();
    private int count = 3;

    private static final TodoService INSTANCE = new TodoService();

    private TodoService() {

    }

    public static TodoService getInstance() {
        return INSTANCE;
    }

    static {
        todoList.add(new Todo(1, "Learn Spring MVC", new Date(9/01/2019), false, "Aziza"));
        todoList.add(new Todo(2, "Finish the book 'The Power of Will'", new Date(8/30/2019), false, "Aziza"));
        todoList.add(new Todo(3, "Read about design patters", new Date(9/15/2019), false, "Aziza"));
    }

    public List<Todo> retrieveItems(String user) {
        List tempList = new ArrayList();
        for (Todo item : todoList) {
            if (item.getUser().equals(user)) {
                tempList.add(item);
            }

        }
        return tempList;
    }

    public boolean addTodo(String description, Date dueDate, boolean isDone, String user) {
        return todoList.add(new Todo(++count, description, dueDate, isDone, user));
    }

    public Todo retrieveItem(int id) {
        for (Todo item: todoList) {
            if (item.getId() == id ) {
                return item;
            }
        }
        return null;
    }

    public void editTodo(Todo todo) {
        deleteItemById(todo.getId());
        todoList.add(todo);
    }

    public void deleteItemById(int id) {

        Iterator iterator = todoList.iterator();

        while (iterator.hasNext()) {
            Todo todo = retrieveItem(id);
            if (iterator.next().equals(todo)) {
                iterator.remove();
            }
        }

    }
}

package com.springmvc.todo;

import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.Size;
import java.util.Date;

public class Todo {

    private int id;

    @Size(min = 2, max = 14)
    private String description;
    private Date dueDate;

    @AssertFalse
    private boolean isDone;

    private String user;

    public Todo(){
        super();
    }

    public Todo(int id, String description, Date dueDate, boolean isDone, String user) {
        this.id = id;
        this.description = description;
        this.dueDate = dueDate;
        this.isDone = isDone;
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}

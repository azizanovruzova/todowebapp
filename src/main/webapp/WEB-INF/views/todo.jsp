<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>

<div class="container">
    <table class="table table-striped">
        <thead>
            Here are your to-do items: <br/>
        </thead>
        <tbody>
            <tr>
                <th>Description</th>
                <th>Due Date</th>
                <th>Is done</th>
                <th>Edit/Delete</th>
            </tr>

            <c:forEach items="${todoList}" var="item">
                <tr>
                    <td>${item.description}</td>
                    <td><fmt:formatDate value="${item.dueDate}" pattern="dd/mm/yyyy" /></td>
                    <td>${item.done}</td>
                    <td><a href="edit-todo?id=${item.id}" class="btn btn-primary">Edit item</a>
                    <a href="delete-todo?id=${item.id}" class="btn btn-danger">Delete item</a></td>
                </tr>
            </c:forEach>



        </tbody>

    </table>
        <br/>
    <a href="add-todo">Add a todo-item to the list</a>

</div>

<%@ include file="common/footer.jspf" %>


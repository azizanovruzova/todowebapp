<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>

    <div class="container">
        <h4>Add an item to the todo list.</h4>
        <form:form method="post" commandName="todo">
            <form:hidden path="id" />
            <fieldset class="form-group">
                <form:label path="description">Description:</form:label>
                <form:input path="description" type="text" class="form-group"/>
                <form:errors path="description" cssClass="text-warning"/>
            </fieldset>

            <fieldset class="form-group">
                <form:label path="dueDate">Due Date:</form:label>
                <form:input path="dueDate" type="text" class="form-group datepicker" id="dueDate"/>
                <form:errors path="dueDate" cssClass="text-warning"/>
            </fieldset>

            <input type="submit" value="Submit" class="btn btn-success"/>
        </form:form>
    </div>

<%@ include file="common/footer.jspf" %>

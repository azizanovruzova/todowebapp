<%@ include file="common/header.jspf" %>

    <div class="container">
        <h4>Please Sign in: </h4>
        <br/>

        <c:if test="${not empty errMsg}"><div style="color:red; font-weight: bold; margin: 30px 0px;">${errMsg}</div></c:if>

        <form name="login" action="${pageContext.request.contextPath}/login" method='POST'>
            <fieldset class="form-group">
                <label>Username:</label>
                <input type="text" name="username" />
            </fieldset>

            <fieldset class="form-group">
                <label>Password:</label>
                <input type="password" name="password" />
            </fieldset>
            <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />--%>
            <input type="submit" value="Sign in" class="btn btn-success"/>
        </form>
    </div>

<%@ include file="common/footer.jspf" %>
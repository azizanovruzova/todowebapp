<%@ include file="common/header.jspf" %>
<%@ include file="common/navigation.jspf" %>

    <div class="container">
        <h4>Welcome to the to do app! </h4>
        <a href="todo" class="btn-link">Go to your to-do list.</a>
    </div>

<%@ include file="common/footer.jspf" %>

